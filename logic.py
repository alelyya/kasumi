from tables import fourier_table, real_table
from numpy import dot, ones, array
from itertools import combinations, chain
from parsing import Polynomial
from misc import lazy_property


class Function:
    '''
    Интерфейс для рассчета и хранения характеристик функции (self.function)
    self.function должна поддерживать __call__(x : list)
    '''

    def __init__(self, n, function):
        self.n = n
        self.function = function

    def __call__(self, x):
        if isinstance(x, int):
            x = list(map(int, bin(x)[2:].zfill(self.n)))
        return self.function(x)

    def __str__(self):
        return str(self.function)

    def __len__(self):
        return len(self.function)

    @lazy_property
    def output(self):
        output = []
        for i in range(2 ** self.n):
            output.append(self(i))
        return output

    @lazy_property
    def weight(self):
        return sum(self.output)

    @lazy_property
    def power(self):
        return self.function.power

    @lazy_property
    def probabilities(self):
        '''Список вероятностей совпадения функции с линейной (аффинной) для всех линейных аналогов.'''
        length = 2 ** self.n
        f_hat = array([sum(self(x) * (-1) ** dot(alpha, x) for x in range(length)) for alpha in range(length)])
        return (ones(length) * 0.5) - f_hat / (length)

    @lazy_property
    def fourier_(self):
        ''' Коэффициенты Фурье. Старая версия, используйте Function.fourier.'''

        def dot_product(v: int, u: int):
            return bin(v & u).count('1') % 2

        m = 2 ** self.n
        f = self.output
        fourier_coefficients = []
        cache = dict()

        for a in range(m):
            summ = 0
            for x in range(m):
                if f[x]:
                    if (a, x) in cache.keys():
                        summ += cache[(a, x)]
                    else:
                        if dot_product(a, x):
                            summ -= 1
                            cache[(a, x)] = -1
                            cache[(x, a)] = -1
                        else:
                            summ += 1
                            cache[(a, x)] = 1
                            cache[(x, a)] = 1
            fourier_coefficients.append(summ)
        return fourier_coefficients

    @lazy_property
    def fourier(self):
        '''Коэффициенты Фурье через матрицу.'''
        return dot(fourier_table(self.n), self.output)

    @lazy_property
    def real(self):
        '''Коэффициенты действительного многочлена.'''
        return dot(real_table(self.n), self.output)

    @lazy_property
    def hadamard_walsh(self):
        '''Коэффициенты Адамара-Уолша.'''
        return dot(fourier_table(self.n), ones(2 ** self.n) - 2*self.fourier)

    @lazy_property
    def real_polynomial(self):
        '''Коэффициенты действительного многочлена.'''

        def beatify(coeff, monomial):
            if coeff == 0:
                return ''
            if int(coeff) == float(coeff):
                coeff = str(int(coeff))
            if monomial == '':
                return str(coeff)
            if abs(float(coeff)) != 1:
                monomial = str(coeff) + '*' + monomial
            return monomial

        monomials = []
        for k in range(self.n+1):
            monomials.extend(combinations(range(self.n), k))
        assert len(self.real) == len(monomials)

        substrings = []
        for coeff, monomial in zip(self.real, monomials):
            monomial = '*'.join(['x'+str(num) for num in monomial])
            substring = beatify(coeff, monomial)
            if substring:
                substrings.append(substring)

        return Polynomial(' + '.join(substrings), sep = '+')  # TODO: Разобраться с парсингом отрицательных чисел.


class Gate:
    '''
    Реализация таблицы подстановок через логические функции (self.gates : list[Function])
    '''

    def __init__(self, functions):
        self.dim = len(functions)
        self.gates = [Function(n = self.dim, function = subfunction) for subfunction in functions]

    def __call__(self, x):
        x = list(map(int, bin(x)[2:].zfill(self.dim)))[::-1]
        return int(''.join(str(x) for x in [gate(x) for gate in self.gates][::-1]), 2)


def intersection(polynomials, sep = '^'):
    monomials = list(set(chain.from_iterable(p.monomials for p in polynomials)))
    string = sep.join(m.string for m in monomials)
    return Polynomial(string, sep)
