import unittest
from parsing import Polynomial, Monomial
import logic
import tables as tl
import numpy as np


class TestParsing(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestParsing, self).__init__(*args, **kwargs)
        self.x = [1, 2, 3, 4, 5, 6]

    def test_monomial(self):

        m = Monomial('x1')
        self.assertEqual(m(self.x), 2)  # x[1] = 2

        m = Monomial('x1*x2')
        self.assertEqual(m(self.x), 6)  # 2 * 3 = 6

        m = Monomial('x0*x1*x2*x3*x4*x5')
        self.assertEqual(m(self.x), 720)  # 6! = 720

        m = Monomial('x0+x1+x5', sep = '+')
        self.assertEqual(m(self.x), 9)  # 1 + 2 + 6 = 9

    def test_polynomial(self):

        p = Polynomial('x1*x2 ^ x0*x1*x2*x3*x4*x5')
        self.assertEqual(p(self.x), 726)  # 6 ^ 720 = 726

        p = Polynomial('x1*x2 ^ x4*x5 ^ x1 * x5')
        self.assertEqual(p(self.x), 20)   # 2*3 ^ 5*6 ^ 2 * 6 = 20

        p = Polynomial('x1*x2 ^ 1')
        self.assertEqual(p(self.x), 7)   # 2*3 ^ 1 = 7

        p = Polynomial('x1&x2 + x4&x5', sep='+')
        self.assertEqual(p(self.x), 6)  # 2 + 4 = 6

    def test_intersect(self):

        a = Polynomial('x1*x2 ^ x4*x5', sep='^')
        b = Polynomial('x3*x4 ^ x4*x5', sep='^')

        x = logic.Function(6, logic.intersection((a,b)))
        y = logic.Function(6, Polynomial('x1*x2 ^ x3*x4 ^ x4*x5'))

        self.assertEqual(x.output, y.output)


class TestSubstitution(unittest.TestCase):

    def test_S7(self):
        S7_gate = logic.Gate(tl.s7_y)
        for x, y in enumerate(tl.S7):
            self.assertEqual(S7_gate(x), y)

    def test_S9(self):
        S9_gate = logic.Gate(tl.s9_y)
        for x, y in enumerate(tl.S9):
            self.assertEqual(S9_gate(x), y)


class TestFunction(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestFunction, self).__init__(*args, **kwargs)
        self.f1 = logic.Function(n = 2, function = Polynomial('x0 ^ x1'))
        self.f2 = logic.Function(n = 3, function = Polynomial('x0 * x1 ^ x2', sep = '*'))
        self.f3 = logic.Function(7, Polynomial('x1*x2 ^ x0*x1*x3 ^ x0*x4 '
                                                 '^ x1*x5 ^ x3*x5 ^ x6 ^ x0*x1*x6'
                                                 '^ x2*x3*x6 ^ x1*x4*x6 ^ x0*x5*x6'))

    def test_weight(self):
        self.assertEqual(self.f1.weight, 2)
        self.assertEqual(self.f3.weight, 64)

    def test_output(self):
        self.assertEqual(self.f1.output, [0, 1, 1, 0])
        self.assertEqual(self.f2.output, [0, 0, 0, 0, 0, 1, 1, 0])

    def test_fourier_(self):
        np.testing.assert_array_equal(self.f1.fourier_, [2, 0, 0, -2])
        np.testing.assert_array_equal(self.f2.fourier_, [2, 0, 0, -2, -2, 0, 0, 2])

    def test_fourier(self):
        np.testing.assert_array_equal(self.f1.fourier, [2, 0, 0, -2])
        np.testing.assert_array_equal(self.f2.fourier, [2, 0, 0, -2, -2, 0, 0, 2])

    def test_compare_fourier(self):
        np.testing.assert_array_equal(self.f1.fourier, self.f1.fourier_)
        np.testing.assert_array_equal(self.f2.fourier, self.f2.fourier_)
        np.testing.assert_array_equal(self.f3.fourier, self.f3.fourier_)

    def test_real(self):
        print(self.f1.function, ' real coefficients: ', self.f1.real)
        print(self.f2.function, ' real coefficients: ', self.f2.real)

    def test_hadamard_walsh(self):
        print(self.f1.function, ' hadamard-walsh coefficients: ', self.f1.hadamard_walsh)
        print(self.f2.function, ' hadamard-walsh coefficients: ', self.f2.hadamard_walsh)

    def test_real_polynomial(self):
        print(self.f1.real_polynomial)
        print(self.f2.real_polynomial)

    def test_power(self):
        f3 = logic.Function(n = 2, function = Polynomial('1 + x0', sep = '+'))
        f4 = logic.Function(n = 2, function = Polynomial('1', sep = '+'))
        self.assertEqual(self.f1.power, 1)
        self.assertEqual(self.f2.power, 2)
        self.assertEqual(self.f3.power, 3)
        self.assertEqual(f3.power, 1)
        self.assertEqual(f4.power, 0)

    def test_probabilities(self):
        # TODO: Проверить правильность расчета вероятнотей.
        print('Вероятности совпадения разрядной функции f1 с линейной: ', self.f1.probabilities)
        print('Вероятности совпадения разрядной функции f2 с линейной: ', self.f2.probabilities)
        print('Вероятности совпадения разрядной функции f3 с линейной: ', self.f3.probabilities)


class TestTables(unittest.TestCase):

    def test_real_table(self):
        np.testing.assert_array_equal(tl.real_table(1), [[1, 0], [-1, 1]])

        np.testing.assert_array_equal(tl.real_table(2), [[1, 0, 0, 0],
                                                         [-1, 1, 0, 0],
                                                         [-1, 0, 1, 0],
                                                         [1, -1, -1, 1]])

    def test_fourier_table(self):
        np.testing.assert_array_equal(tl.fourier_table(1), [[1, 1], [1, -1]])

        np.testing.assert_array_equal(tl.fourier_table(2), [[1, 1, 1, 1],
                                                            [1, -1, 1, -1],
                                                            [1, 1, -1, -1],
                                                            [1, -1, -1, 1]])


if __name__ == '__main__':
    unittest.main()
