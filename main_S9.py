from tables import s9_y
from logic import Function, intersection


s9_functions = [Function(9, y) for y in s9_y]
s9_real_polynomials = [f.real_polynomial for f in s9_functions]
s9_intersection = intersection(s9_y)
s9_real_intersection = intersection(s9_real_polynomials)

for n, f in enumerate(s9_functions):
    print('y{}: {}'.format(n, f))
    print('Вес: ', f.weight)
    print('Число мономов: ', len(f))
    print('Коэффициенты Фурье: ', f.fourier)
    print('Коэффициенты Адамара-Уолша', f.hadamard_walsh)
    print('Коэффициенты действительного многочлена', f.real)

    f_real = s9_real_polynomials[n]
    print('Действительный многочлен: ', f_real)
    print('Число мономов в действительном многочлене: ', len(f_real))
    print('Степень действительного многочлена: ', f_real.power)

    print('Линейные аналоги и соответствующие вероятности совпадения разрядной функции с линейной: ')
    for alpha, prob in enumerate(f.probabilities):
        print('({}: {})'.format(bin(alpha)[2:].zfill(f.n), prob), end=' ')

    print('\n---------------------------------------------------\n')


print('Число мономов во всех многочленах Жегалкина:', len(s9_intersection))
print('Число мономов во всех действительных многочленах:', len(s9_real_intersection))
