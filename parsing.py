class Variable:
    def __init__(self, string):
        self.type = 'num'
        if 'x' in string:
            self.type = 'var'

        self.string = string.replace('x', '').replace(' ', '')
        self.power = 0 if self.type == 'num' else 1

    def __call__(self, x):
        if self.type == 'num':
            return float(self.string)
        return x[int(self.string)]


class Monomial:
    '''
    Представление монома (x1 * x2 * x3) в виде (1 * 2 * 3),
    где * - операция, 1,2,3 - координаты произвольного вектора.

    TODO: проверка длины вектора x при вызове __call__(x).
    '''

    def __init__(self, string, sep = '*'):
        self.string = string
        self.sep = sep

        # проверка на правильность sep (выделение нового при необходимости)
        if sep not in string:
            new_sep = string.translate({ord(i): None for i in 'x 1234567890'})  # todo: ^^^^^^ -> ^
            if new_sep:
                self.sep = new_sep

        self.operation = lambda a, b: eval('a'+self.sep+'b')
        self.parts = [Variable(i) for i in self.string.split(self.sep)]

    def __eq__(self, other):
        return self.string == other.string

    def __call__(self, x):

        if len(self.string) == 1:  # When monomial is just a number.
            return int(self.string)

        result = None
        for part in self.parts:
            if result is None:
                result = part(x)
                continue
            result = self.operation(result, part(x))
        return result

    def __str__(self):
        return "'" + self.string + "'"

    def __hash__(self):
        return hash(self.string)

    @property
    def power(self):
        return sum([variable.power for variable in self.parts])


class Polynomial:
    '''
    Представление полинома в виде применения операции (self.operation) к нескольким мономам.
    '''

    def __init__(self, string, sep = '^'):
        self.string = string
        self.sep = sep
        self.monomials = [Monomial(s) for s in self.string.replace(' ', '').split(self.sep)]
        self.operation = lambda a, b: eval('a'+self.sep+'b')

    def __eq__(self, other):
        raise NotImplementedError

    def __call__(self, x):
        result = None
        for monomial in self.monomials:
            if result is None:
                result = monomial(x)
                continue
            result = self.operation(result, monomial(x))
        return result

    def __len__(self):
        return len(self.monomials)

    def __str__(self):
        return "'" + self.string + "'"

    def __hash__(self):
        return hash(self.string)

    @property
    def power(self):
        return max([monomial.power for monomial in self.monomials])
