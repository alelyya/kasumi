from tables import s7_y, s9_y
from logic import Function, intersection
from parsing import Polynomial


def linear_combinations(polynomials):
    n = len(polynomials)

    for mask in range(2 ** n):
        if mask == 0:
            yield '0'*n, Polynomial('0')
            continue

        mask = bin(mask)[2:].zfill(n)

        combination = []

        for num, value in enumerate(mask):
            if value == '1':
                combination.append(polynomials[num])

        yield mask, intersection(combination)


for mask, comb in linear_combinations(s7_y):
    f = Function(7, comb)
    print('Вес ', f, ' ', f.weight)

for mask, comb in linear_combinations(s9_y):
    f = Function(9, comb)
    print('Вес ', f, ' ', f.weight)
