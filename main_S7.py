from tables import s7_y
from logic import Function, intersection


s7_functions = [Function(7, y) for y in s7_y]
s7_real_polynomials = [f.real_polynomial for f in s7_functions]
s7_intersection = intersection(s7_y)
s7_real_intersection = intersection(s7_real_polynomials)

for n, f in enumerate(s7_functions):
    print('y{}: {}'.format(n, f))
    print('Вес: ', f.weight)
    print('Число мономов: ', len(f))
    print('Коэффициенты Фурье: ', f.fourier)
    print('Коэффициенты Адамара-Уолша', f.hadamard_walsh)
    print('Коэффициенты действительного многочлена', f.real)

    f_real = s7_real_polynomials[n]
    print('Действительный многочлен: ', f_real)
    print('Число мономов в действительном многочлене: ', len(f_real))
    print('Степень действительного многочлена: ', f_real.power)

    print('Линейные аналоги и соответствующие вероятности совпадения разрядной функции с линейной: ')
    for alpha, prob in enumerate(f.probabilities):
        print('({}: {})'.format(bin(alpha)[2:].zfill(f.n), prob), end=' ')

    print('\n---------------------------------------------------\n')


print('Число мономов во всех многочленах Жегалкина:', len(s7_intersection))
print('Число мономов во всех действительных многочленах:', len(s7_real_intersection))
